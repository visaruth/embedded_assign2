#include<reg51.h>
sbit latch = P1^4;
unsigned char led[] = {0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0xA,0xB,0xC,0xD,0xE,0xF};
int m = 0;

void delay(int tick){
	int i,j;
	for(i=0;i<tick;i++)
		for(j=0;j<20;j++);
}

void main(void){
	latch = 1;
	while(1){
		for(m=0;m<16;m++){
			P0=led[m];
			delay(1000);
		}
	}
}